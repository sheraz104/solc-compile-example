import solc from 'solc';
import fs from 'fs';
import path from 'path';

const logger = {
  log (...arg) {
    console.log(arg);
  },
  info (...arg) {
    console.log(arg);
  },
  error (...arg) {
    console.log(arg);
  }
};

function getContract (contractPath) {
  return fs.readFileSync(
    path.join(
      process.cwd(),
      contractPath,
    ),
    'utf-8',
  );
}

function findImport (contractName) {
  try {
    return {
      contents: getContract(
        contractName,
      ),
    };
  } catch (e) {
    return {
      error: `${contractName} not found!!`,
    };
  }
}

function getSolcInput () {
  return {
    language: 'Solidity',
    sources: {
      'TestContract.sol': {
        content: getContract('TestContract.sol'),
      }
    },
    settings: {
      outputSelection: {
        '*': {
          '*': ['*']
        }
      }
    }
  };
}

function compileWithSolc () {
  const compiledOutput = solc.compile(
    JSON.stringify(getSolcInput()),
    {
      import: findImport,
    });

  return Object.values(
    JSON.parse(compiledOutput).contracts
  ).reduce(
    (acc, contract) => {
      const {
        abi,
        evm: {bytecode: {object: bytecode}}
      } = Object.values(contract)[0];

      return [...acc, {
        bytecode,
        abi: JSON.stringify(
          abi,
          null,
          ' ',
        ),
      }];
    },
    []);
}

function main () {
  try {
    const contracts = compileWithSolc();
    logger.log(contracts);
  } catch (e) {
    logger.error('Error: ', e.message);
  }
}

main();
