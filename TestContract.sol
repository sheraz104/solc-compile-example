pragma solidity 0.6.6;


import './BaseContract/TestOneContract.sol';


contract TestContract is TestOneContract {
    string private name;

    constructor (string memory _name) public {
        name = _name;
    }

    function getName()
    public
    view
    returns (string memory) {
        return name;
    }
}
